<?php

namespace Bitrix24RestSdk\Entity2;

use Bitrix24RestSdk\Core\ServiceProvider;
use Bitrix24RestSdk\Entity2\AbstractEntity;
use Bitrix24RestSdk\Core\Traits\Base;
use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Services\UserService;

#[\AllowDynamicProperties]
class Product extends AbstractEntity
{

    public function Get($id): Product
    {
        $product = clone $this;
        $product->data = $this->api->Product()->Get($id);
        return $product;
    }
    public function Create(): bool|int
    {
        return 1;
    }
    public function Update($parm, $id = null): bool
    {
        return 1;
    }
    public function Delete($id = null): bool
    {
        return 1;
    }
}
