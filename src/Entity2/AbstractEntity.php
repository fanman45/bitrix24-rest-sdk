<?php

namespace Bitrix24RestSdk\Entity2;

use Bitrix24RestSdk\Core\Handlers\Property;
use Bitrix24RestSdk\Core\ServiceProvider;
use Bitrix24RestSdk\Core\EntityProvider;
use Bitrix24RestSdk\Implementation\ApiRegistry;

abstract class AbstractEntity
{
    public $entityId;
    public $entityTypeId;
    // protected array $property;
    // public $fieldsType;
    // public static ServiceProvider $api;
    public array $entityMode;
    // public $fields;
    public array $props;
    public const propValMap = [];
    public const PROP_ENTYTY_MAP = [];

    public function __construct(public ServiceProvider $api, public EntityProvider $eapi, public array $fieldsType, protected array $propValMap = [], $thin = true, public array|null $data = null)
    {
        // $this->entityId = $this->ID->value;
        // static::PROP_VAL_MAP =  $propValMap;
        // static::$api = $api;
        // $this->data = $data;

        // $this->fieldsType = $fieldsType;
        // $this->entityMode = $entityMode;
        // $this->fields = $fields['fields'];
        // dd($this->fields);
        // $camelCaseProp = $this->CamelCaser($result);
        // $this->SetDefProp($result);
        // $this->property = $this->SetProp($result);

        // if (static::PROP_VAL_MAP) {
        //     $this->SetCustomProp($result);
        // }
        // if (static::PROP_ENTYTY_MAP) {
        //     $this->ReplaceValProp();
        // }
    }

    public function __get($name)
    {
        \Debugbar::startMeasure("getProp");
        if (!isset($this->props[$name])) {
            if (array_key_exists($name,$this->data)) {
                $this->props[$name] = new Property($this, $name);
            } elseif (array_key_exists($name,$this->propValMap)) {
                if (array_key_exists($this->propValMap[$name],$this->data)) {
                    $this->props[$name] = new Property($this, $this->propValMap[$name]);
                } else {
                    throw new \Exception('Кастомное свойство ' . $name . ' задано в параметрах, но значение на которое оно ссылается ' . $this->propValMap[$name] . ' отсутствует в сущности');
                }
            } else {
                throw new \Exception('Свойство отсутствует');
            }
        }
        return $this->props[$name];
        \Debugbar::stopMeasure("getProp");
    }

    public function SetDefProp($propArray)
    {
        foreach ($propArray as $name => $propVal) {
            if (\property_exists($this, $name)) {

                // dd($name , $propVal);
                $this->$name = $propVal;
            }
        }
    }

    protected function SetCustomProp(array $origin)
    {
        foreach ($this->propValMap as $customKey => $originKey) {
            if (\array_key_exists($originKey, $origin)) {
                $this->$customKey = $origin[$originKey];
            }
        }
    }

    /**
     * Проходится по свойствам объекта, ищет в массиве для замены свойство и подставляет значение, преобразует массивы в объекты
     * Нужно проверить обратную реализацию
     * @return [type]
     */
    protected function ReplaceValProp()
    {

        $entityMap = static::PROP_ENTYTY_MAP;
        $props = \get_object_vars($this);
        // dd($props);
        unset($props['raw']);
        foreach ($props as $prop => $val) {
            // проверка на наличие заменяемго свойства
            if (\array_key_exists($prop, $entityMap)) {
                // Проверка на наличие спец свойства
                if (key($entityMap[$prop]) == 'replace') {
                    // Обработка массивов и преобразование в объекты
                    if (\is_array($this->$prop)) {
                        // $this->$prop = (object)$this->$prop;
                        $tmpArray = [];
                        foreach ($this->$prop as $key => $val) {
                            // $this->$prop[$key] = $entityMap[$prop]['replace'][$val];
                            $tmpArray[$key] = $entityMap[$prop]['replace'][$val];
                        }
                        $this->$prop = (object)$tmpArray;
                    } else {
                        // Проверка ! вылетала ошибка н отсутствие ключа
                        // if (isset($entityMap[$prop]['replace'][$val])) {
                        $this->$prop = (isset($entityMap[$prop]['replace'][$val])) ? $entityMap[$prop]['replace'][$val] : $val;
                        // }
                        // dd($this->$prop,$prop, $val,$entityMap[$prop]['replace'][$val]);
                    }
                } elseif (key($entityMap[$prop]) == 'entity') {
                    // Необходимо дописать
                    if ($entityMap[$prop]['entity'] == 'User') {
                        if (\is_array($this->$prop)) {
                            // $this->$prop = (object)$this->$prop;
                            $tmpArray = [];
                            foreach ($this->$prop as $key => $val) {
                                // dd($this->$prop,$this->getUser($val));
                                $tmpArray[] = $this->getUser($val);
                                // $this->$prop[$key] = $entityMap[$prop]['replace'][$val];
                                // $tmpArray[$key] = $entityMap[$prop]['replace'][$val];
                            }
                            // dd($tmpArray);
                            $this->$prop = (object)$tmpArray;
                        } else {
                            $this->$prop = $this->getUser($this->$prop);
                        }
                    }
                    if ($entityMap[$prop]['entity'] == 'Company') {
                        if (\is_array($this->$prop)) {
                            $tmpArray = [];
                            foreach ($this->$prop as $key => $val) {
                                $tmpArray[] = $this->getCompany($val);
                            }
                            $this->$prop = (object)$tmpArray;
                        } else {
                            $this->$prop = $this->getCompany($this->$prop);
                        }
                    }
                }
            }
            // Превращаем нетронутые массивы в объекты
            if (\is_array($val)) {
                $this->$prop = (object)$val;
            }
        }
    }

    protected function CamelCaser(array $prop)
    {
        // settype($test,string)
        $parm = [];
        foreach ($prop as $key => $val) {
            $parm[\lcfirst(\str_replace('_', '', \ucwords(\strtolower($key), '_')))] = $val;
        }
        return $parm;
    }

    // public function __get($property)
    // {
    //     return isset($this->property[$property]) ? $this->property[$property] : null;
    // }

    protected function SetProp(array $prop)
    {
        $parm = [];
        foreach ($prop as $key => $val) {
            $parm[lcfirst(str_replace('_', '', ucwords(strtolower($key), '_')))] = $val;
        }
        return $parm;
    }
    // public function toArray(){
    //     return $this->property;
    // }
    // public function toRaw(){
    //     return $this->property['raw'];
    // }

    /**
     * перенести в сам сервис
     * @param array|string|object $id
     * 
     * @return [type]
     */
    // public function getUser(array|string|object $id)
    // {
    //     $user =  new UserService(static::$api);
    //     $user->Get($id);
    //     if (is_array($id)) {
    //         $id = (object) $id;
    //     }

    //     if (empty($id)) {
    //         return '';
    //     }

    //     if (is_object($id)) {
    //         $result = [];
    //         foreach ($id as $i) {
    //             $result[] = $user->Get($i);
    //         }
    //         return (object)$result;
    //     } else {
    //         return $user->Get($id);
    //     }
    // }
    // public function getCompany(array|string|object $id)
    // {
    //     $company =  new CompanyService(static::$api);
    //     $company->Get($id);
    //     if (is_array($id)) {
    //         $id = (object) $id;
    //     }

    //     if (empty($id)) {
    //         return '';
    //     }

    //     if (is_object($id)) {
    //         $result = [];
    //         foreach ($id as $i) {
    //             $result[] = $company->Get($i);
    //         }
    //         return (object)$result;
    //     } else {
    //         return $company->Get($id);
    //     }
    // }
}
