<?php

namespace Bitrix24RestSdk\Entity2;

use Bitrix24RestSdk\Core\ServiceProvider;
use Bitrix24RestSdk\Core\EntityProvider;
use Bitrix24RestSdk\Entity2\AbstractEntity;
use Bitrix24RestSdk\Core\Traits\Base;
use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Services\UserService;


#[\AllowDynamicProperties]
class CRMEntity extends AbstractEntity
{
    public function __construct(public ServiceProvider $api, public EntityProvider $eapi, public array $fieldsType, public $entityTypeId, protected array $propValMap = [], public bool $thin = true, public array|null $data = null)
    {
        parent::__construct($api, $eapi, $fieldsType, $propValMap);
        $this->entityTypeId = $entityTypeId;
    }

    public function Get($id): CRMEntity
    {
        $entity = clone $this;
        // dd($this->api->SMItem()->Get($this->entityTypeId, $id));
        $entity->data = $this->api->SMItem()->Get($this->entityTypeId, $id)['item'];
        $entity->entityId = $entity->id->valueRaw;
        return $entity;
    }
    public function List(): array
    {
        $result = [];
        foreach($this->api->SMItem()->List($this->entityTypeId)['items'] as $item){
            $result[] = $this->Get($item['id']);
        }
        return $result;
    }
    public function ListM(array|null $select = null, array|null $order = null, array|null $filter = null, int|null $start = 0): array
    {   
        $result = [];
        foreach($this->api->SMItem()->List($this->entityTypeId,$select,$order,$filter,$start)['items'] as $item){
            // $result[] = $this->Get($item['id']);
            $entity = clone $this;
            $entity->data = $item;
            $entity->entityId = $entity->id->valueRaw;
            $result[] = $entity;
        }
        return $result;
    }
    public function ListArray(): array
    {
        return $this->api->SMItem()->List($this->entityTypeId)['items'];
    }
    public function Create(): bool|int
    {
        return 1;
    }
    public function Update($parm, $id = null): bool
    {
        return 1;
    }
    public function Delete($id = null): bool
    {
        return 1;
    }
}
