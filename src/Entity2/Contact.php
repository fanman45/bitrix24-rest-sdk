<?php

namespace Bitrix24RestSdk\Entity2;

use Bitrix24RestSdk\Core\ServiceProvider;
use Bitrix24RestSdk\Entity2\AbstractEntity;
use Bitrix24RestSdk\Core\Traits\Base;
use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Services\UserService;

#[\AllowDynamicProperties]
class Contact extends AbstractEntity
{
    // public $entityTypeId = 2;

    public function Get($id): Contact
    {

        $entity = clone $this;
        $entity->data = $this->api->Contact()->Get($id);
        $entity->entityId = $entity->ID->valueRaw;
        return $entity;
    }
    public function Create(): bool|int
    {
        return 1;
    }
    public function Update($parm, $id = null): bool
    {
        return 1;
    }
    public function Delete($id = null): bool
    {
        return 1;
    }
}
