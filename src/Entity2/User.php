<?php

namespace Bitrix24RestSdk\Entity2;

use Bitrix24RestSdk\Core\ServiceProvider;
use Bitrix24RestSdk\Entity2\AbstractEntity;
use Bitrix24RestSdk\Core\Traits\Base;
use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Services\UserService;

#[\AllowDynamicProperties]
class User extends AbstractEntity
{
    // public $entityTypeId = 2;

    public function Get($id): User
    {
        // \Debugbar::startMeasure("user");
        $user = clone $this;
        $user->data = $this->api->User()->Get($id)[0];
        // dd($user);
        $user->entityId = $user->ID->valueRaw;
        // dd($user);
        // \Debugbar::stopMeasure("user");
        return $user;
       
    }
    public function Create(): bool|int
    {
        return 1;
    }
    public function Update($parm, $id = null): bool
    {
        return 1;
    }
    public function Delete($id = null): bool
    {
        return 1;
    }

    public function fullName()
    {
        // dd($this->NAME);
        return $this->NAME->valueRaw . " " . $this->LAST_NAME->valueRaw;
    }
}
