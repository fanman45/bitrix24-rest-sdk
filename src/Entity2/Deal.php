<?php

namespace Bitrix24RestSdk\Entity2;

use Bitrix24RestSdk\Core\ServiceProvider;
use Bitrix24RestSdk\Entity2\AbstractEntity;
use Bitrix24RestSdk\Core\Traits\Base;
use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Implementation\ApiRegistry;
use Bitrix24RestSdk\Services\UserService;

#[\AllowDynamicProperties]
class Deal extends AbstractEntity
{
    public $entityTypeId = 2;

    public function Get($id): Deal
    {
        $deal = clone $this;
        $deal->data = $this->api->Deal()->Get($id);
        $deal->entityId = $deal->ID->valueRaw;
        return $deal;
    }
    public function Create(): bool|int
    {
        return 1;
    }
    public function Update($parm, $id = null): bool
    {
        return 1;
    }
    public function Delete($id = null): bool
    {
        return 1;
    }
    public function Products()
    {
        return $this->api->Deal()->ProductrowsGet($this->entityId);
    }
}
