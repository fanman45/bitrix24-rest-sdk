<?php

namespace Bitrix24RestSdk\Entity;

use Bitrix24RestSdk\Entity\AbstractEntity;
use Bitrix24RestSdk\Implementation\ApiClient;

class Company extends AbstractEntity
{
    public $id;
    public $title;
    public function __construct($result = null, ApiClient $api)
    {
        $camelCaseProp = $this->CamelCaser($result);

        parent::__construct($camelCaseProp, $api);
    }
}
