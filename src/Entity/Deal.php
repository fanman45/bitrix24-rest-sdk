<?php

namespace Bitrix24RestSdk\Entity;

use Bitrix24RestSdk\Entity\AbstractEntity;
use Bitrix24RestSdk\Core\Traits\Base;
use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Services\UserService;

#[\AllowDynamicProperties]
class Deal extends AbstractEntity
{
    // // Мапинг кастомных полей наследуем и дополняем
    // protected const PROP_VAL_MAP = [
    //     'main' => 'ufCrm1679062465120',
    //     'assigner' => 'ufCrm1657100484',
    //     'line' => 'ufCrm1661868372',
    //     'product' => 'ufCrm1661868409',
    //     'lawyer' => 'ufCrm1661868386',
    //     'architect' => 'ufCrm1661868665',
    //     'projectId' => 'parentId175',
    //     'buildingId' => 'parentId171'
    // ];

    // protected const PROP_ENTYTY_MAP = [
    //     'main' => [
    //         'replace' => [
    //             795 => true,
    //             796 => false
    //         ]
    //     ],
    //     'opened' => ['replace' => self::YN],
    //     'closed'  => ['replace' => self::YN],
    //     'isManualOpportunity' => ['replace' => self::YN],
    //     // 'assigner' => [
    //     //     'replace' => [
    //     //         706 => "gthdsq",
    //     //         13 => 'dfaa'
    //     //     ]
    //     // ],
    //     'assigner' => [
    //         'entity' => 'User'
    //     ],
    //     'line' => [
    //         'entity' => 'User'
    //     ]
    // ];
    // protected const YN = [
    //     'Y' => true,
    //     'N' => false
    // ];

    public $id;
    public $title;
    public $stageId;
    public $typeId;
    public $probability;
    public $currencyId;
    public $opportunity;
    public $isManualOpportunity;
    public $taxValue;
    public $leadId;
    public $companyId;
    public $quoteId;
    public $begindate;
    public $closedate;
    public $assignedById;
    public $createdById;
    public $modifyById;
    public $dateCreate;
    public $dateModify;
    public $opened;
    public $closed;
    public $comments;
    public $additionalInfo;
    public $locationId;
    public $categoryId;
    public $stageSemanticId;
    public $isNew;
    public $isRecurring;
    public $isReturnCustomer;
    public $isRepeatedApproach;
    public $sourceId;
    public $sourceDescription;
    public $originatorId;
    public $originId;
    public $movedById;
    public $movedTime;
    public $utmSource;
    public $utmMedium;
    public $utmCampaign;
    public $utmContent;
    public $utmTerm;
    public $lastActivityBy;
    public $lastActivityTime;

    public function __construct($result = null, ApiClient $api, $thin = true)
    {
        $camelCaseProp = $this->CamelCaser($result);

        parent::__construct($camelCaseProp, $api);

        // parent::__construct(fn () => $this->CamelCaser($result), $api);

        //     // для тестов кемелкейсинга
        //     parent::__construct($result, $api);
    }
}
