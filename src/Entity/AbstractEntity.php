<?php

namespace Bitrix24RestSdk\Entity;

use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Services\UserService;
use Bitrix24RestSdk\Services\ListsService;
use Bitrix24RestSdk\Services\CRM\Dynamic\ItemService;
use Bitrix24RestSdk\Services\CRM\CompanyService;
use Bitrix24RestSdk\Core\Handlers\Replacer;
use Bitrix24RestSdk\Core\Handlers\UfReplacer;

abstract class AbstractEntity
{

    protected array $property;
    public $raw;
    protected static $api;

    protected const PROP_VAL_MAP = [];
    protected const PROP_ENTYTY_MAP = [];

    public function __construct($result = null, ApiClient $api)
    {
        static::$api = $api;
        $this->raw = $result;
        // $camelCaseProp = $this->CamelCaser($result);
        $this->SetDefProp($result);
        // $this->property = $this->SetProp($result);

        if (static::PROP_VAL_MAP) {
            $this->SetCustomProp($result);
        }
        if (static::PROP_ENTYTY_MAP) {
            $this->ReplaceValProp();
        }
    }

    public function SetDefProp($propArray)
    {
        foreach ($propArray as $name => $propVal) {
            if (\property_exists($this, $name)) {

                // dd($name , $propVal);
                $this->$name = $propVal;
            }
        }
    }

    protected function SetCustomProp(array $origin)
    {
        foreach (static::PROP_VAL_MAP as $customKey => $originKey) {
            if (\array_key_exists($originKey, $origin)) {
                $this->$customKey = $origin[$originKey];
            }
        }
    }

    /**
     * Проходится по свойствам объекта, ищет в массиве для замены свойство и подставляет значение, преобразует массивы в объекты
     * Нужно проверить обратную реализацию
     * @return [type]
     */
    protected function ReplaceValProp()
    {

        $entityMap = static::PROP_ENTYTY_MAP;
        $props = \get_object_vars($this);
        // dd($props);
        unset($props['raw']);
        foreach ($props as $prop => $val) {
            // проверка на наличие заменяемго свойства
            if (\array_key_exists($prop, $entityMap)) {
                // Проверка на наличие спец свойства
                if (key($entityMap[$prop]) == 'replace') {
                    // Обработка массивов и преобразование в объекты
                    if (\is_array($this->$prop)) {
                        // $this->$prop = (object)$this->$prop;
                        $tmpArray = [];
                        foreach ($this->$prop as $key => $val) {
                            // $this->$prop[$key] = $entityMap[$prop]['replace'][$val];
                            $tmpArray[$key] = $entityMap[$prop]['replace'][$val];
                        }
                        $this->$prop = (object)$tmpArray;
                    } else {
                        // Проверка ! вылетала ошибка н отсутствие ключа
                        // if (isset($entityMap[$prop]['replace'][$val])) {
                        $this->$prop = (isset($entityMap[$prop]['replace'][$val]))? $entityMap[$prop]['replace'][$val] : $val;
                        // }
                        // dd($this->$prop,$prop, $val,$entityMap[$prop]['replace'][$val]);
                    }
                } elseif (key($entityMap[$prop]) == 'entity') {
                    // Необходимо дописать
                    if ($entityMap[$prop]['entity'] == 'User') {
                        if (\is_array($this->$prop)) {
                            // $this->$prop = (object)$this->$prop;
                            $tmpArray = [];
                            foreach ($this->$prop as $key => $val) {
                                // dd($this->$prop,$this->getUser($val));
                                $tmpArray[] = $this->getUser($val);
                                // $this->$prop[$key] = $entityMap[$prop]['replace'][$val];
                                // $tmpArray[$key] = $entityMap[$prop]['replace'][$val];
                            }
                            // dd($tmpArray);
                            $this->$prop = (object)$tmpArray;
                        } else {
                            $this->$prop = $this->getUser($this->$prop);
                        }
                    }
                    if ($entityMap[$prop]['entity'] == 'Company') {
                        if (\is_array($this->$prop)) {
                            $tmpArray = [];
                            foreach ($this->$prop as $key => $val) {
                                $tmpArray[] = $this->getCompany($val);
                            }
                            $this->$prop = (object)$tmpArray;
                        } else {
                            $this->$prop = $this->getCompany($this->$prop);
                        }
                    }
                }
                // } elseif(key($entityMap[$prop]) == 'replaceIB'){
                //     $this->$prop = $this->ReplaceIB(new ItemService(static::$api),162,$entityMap[$prop]['replaceIB']);
                //     dd($this->ReplaceIB(new ItemService(static::$api),162,$entityMap[$prop]['replaceIB']));
                // }
            }
            // Превращаем нетронутые массивы в объекты
            if (\is_array($val)) {
                $this->$prop = (object)$val;
            }
        }
    }

    protected function CamelCaser(array $prop)
    {
        // settype($test,string)
        $parm = [];
        foreach ($prop as $key => $val) {
            $parm[\lcfirst(\str_replace('_', '', \ucwords(\strtolower($key), '_')))] = $val;
        }
        return $parm;
    }

    // public function __get($property)
    // {
    //     return isset($this->property[$property]) ? $this->property[$property] : null;
    // }

    protected function SetProp(array $prop)
    {
        $parm = [];
        foreach ($prop as $key => $val) {
            $parm[lcfirst(str_replace('_', '', ucwords(strtolower($key), '_')))] = $val;
        }
        return $parm;
    }
    // public function toArray(){
    //     return $this->property;
    // }
    // public function toRaw(){
    //     return $this->property['raw'];
    // }

    /**
     * перенести в сам сервис
     * @param array|string|object $id
     * 
     * @return [type]
     */
    public function getUser(array|string|object $id)
    {
        $user =  new UserService(static::$api);
        $user->Get($id);
        if (\is_array($id)) {
            $id = (object) $id;
        }

        if (empty($id)) {
            return '';
        }

        if (\is_object($id)) {
            $result = [];
            foreach ($id as $i) {
                $result[] = $user->Get($i);
            }
            return (object)$result;
        } else {
            return $user->Get($id);
        }
    }
    public function getCompany(array|string|object $id)
    {
        $company =  new CompanyService(static::$api);
        $company->Get($id);
        if (\is_array($id)) {
            $id = (object) $id;
        }

        if (empty($id)) {
            return '';
        }

        if (\is_object($id)) {
            $result = [];
            foreach ($id as $i) {
                $result[] = $company->Get($i);
            }
            return (object)$result;
        } else {
            return $company->Get($id);
        }
    }

}
