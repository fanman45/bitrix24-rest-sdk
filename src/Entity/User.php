<?php

namespace Bitrix24RestSdk\Entity;

use Bitrix24RestSdk\Entity\AbstractEntity;
use Bitrix24RestSdk\Core\Interfaces\UserInterface;
use Bitrix24RestSdk\Implementation\ApiClient;

class User extends AbstractEntity
{

    public $id;
    public $active;
    public $name;
    public $lastName;
    public $secondName;
    public $title;
    public $emailirtemail;
    public $lastLogin;
    public $dateRegister;
    public $timeZone;
    public $isOnline;
    public $timeZoneOffset;
    public $timestampX;
    public $lastActivityDate;
    public $personalGender;
    public $personalProfession;
    public $personalWww;
    public $personalBirthday;
    public $personalIcq;
    public $personalPhone;
    public $personalFax;
    public $personalMobile;
    public $personalPager;
    public $personalStreet;
    public $personalCity;
    public $personalState;
    public $personalZip;
    public $personalCountry;
    public $personalMailbox;
    public $personalNotes;
    public $workPhone;
    public $workCompany;
    public $workPosition;
    public $workDepartment;
    public $workWww;
    public $workFax;
    public $workPager;
    public $workStreet;
    public $workMailbox;
    public $workCity;
    public $workState;
    public $workZip;
    public $workCountry;
    public $workProfile;
    public $workNotes;
    public $ufEmploymentDate;
    public $ufDepartment;
    public $userType;

    public function __construct($result = null, ApiClient $api)
    {
        if (isset($result[0])) {
           $camelCaseProp = $this->CamelCaser($result[0]);
        } else {
            $camelCaseProp = $this->CamelCaser($result);
        }
        parent::__construct($camelCaseProp, $api);
    }

    public function fullName()
    {
        return $this->name . "2" . $this->lastName;
    }
}
