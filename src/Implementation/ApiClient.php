<?php

namespace Bitrix24RestSdk\Implementation;

use Bitrix24RestSdk\Core\Interfaces\ApiInterface;
use Bitrix24RestSdk\Core\Interfaces\StorageInterface;
use Bitrix24RestSdk\Core\Interfaces\SessionInterface;

/**
 * Адаптация для использования с Laravel
 *  @version 1.36
 */
class ApiClient implements ApiInterface
{

	const VERSION = '1.36';
	const BATCH_COUNT    = 50; //count batch 1 query
	const TYPE_TRANSPORT = 'json'; // json or xml

	public string|null $domain;
	protected string $mode;
	// protected  string $clientId;
	// protected  string $clientSecret;
	// protected  $webHookUrl;
	// protected  string $currentEncoding;
	// protected  bool $blockLog;
	// protected  $logsDir;
	// protected  bool $logTypeDump;
	// protected  bool $ignoreSsl;
	protected  bool $install;
	//
	protected $portal;
	protected $appSettings;
	protected $settings;
	protected StorageInterface $storage;
	protected SessionInterface $session;
	protected $portalRead;
	protected $settingRead;


	public function __construct(SessionInterface $session, StorageInterface $storage, $mode = 'user')
	{
		// dd($storage, $session->domain);
		$this->storage = $storage;
		$this->session = $session;
		$this->domain = $session->domain;
		$this->mode = $mode;
		$this->install = false;


		$portal = $this->storage->Read('portal', $this->session->domain);
		$setting = $this->storage->Read('setting', $this->session->domain);
		$this->portalRead = $portal;
		$this->settingRead = $setting;

		if (!$this->portal) {
			$this->portal = $this->getPortal();
		}
	}

	/**
	 * Установка приложения
	 * @param Request $request
	 * 
	 * @return [type]
	 */

	public function installApp()
	{

		$_REQUEST['event'] = '';
		$result = [
			'rest_only' => true,
			'install' => false
		];
		if ($_REQUEST['event'] == 'ONAPPINSTALL' && !empty($_REQUEST['auth'])) {
			$result['install'] = $this->setAppSettings($_REQUEST['auth'], true);
		} elseif ($_REQUEST['PLACEMENT'] == 'DEFAULT') {
			$result['rest_only'] = false;
			$result['install'] = $this->setAppSettings(
				[
					'access_token' => htmlspecialchars($_REQUEST['AUTH_ID']),
					'expires_in' => htmlspecialchars($_REQUEST['AUTH_EXPIRES']),
					'application_token' => htmlspecialchars($_REQUEST['APP_SID']),
					'refresh_token' => htmlspecialchars($_REQUEST['REFRESH_ID']),
					'domain' => htmlspecialchars($_REQUEST['DOMAIN']),
					'client_endpoint' => 'https://' . htmlspecialchars($_REQUEST['DOMAIN']) . '/rest/',
				],
				true
			);
		}

		// static::setLog(
		// 	[
		// 		'request' => $_REQUEST,
		// 		'result' => $result
		// 	],
		// 	'installApp'
		// );
		return $result;
	}

	// public static function installAppV2(Request $request)
	// {
	// 	$result = [
	// 		'rest_only' => true,
	// 		'install' => false
	// 	];
	// 	if ($request->event == 'ONAPPINSTALL' && !empty($request->auth)) {
	// 		$result['install'] = static::setAppSettings($request->DOMAIN, $request->auth, true);
	// 	} elseif ($request->PLACEMENT == 'DEFAULT') {

	// 		$config = false;
	// 		if (Portals::existsPortal($request->DOMAIN)) {
	// 			// if(!empty($this->storage->Read('portal', $request->DOMAIN))){
	// 			$config = true;
	// 		}
	// 		$result['rest_only'] = false;
	// 		$result['install'] = $config && static::setAppSettings(
	// 			$request->DOMAIN,
	// 			[
	// 				'access_token' => $request->AUTH_ID,
	// 				'expires_in' => $request->AUTH_EXPIRES,
	// 				'application_token' => $request->APP_SID,
	// 				'refresh_token' => $request->REFRESH_ID,
	// 				'domain' => $request->DOMAIN,
	// 				'client_endpoint' => 'https://' . $request->DOMAIN . '/rest/',
	// 			],
	// 			true
	// 		);
	// 	}

	// 	// Debugbar::info('installApp', $result);
	// 	// Log::channel('devlog')->notice(serialize($request->toArray()));
	// 	// Log::channel('devlog')->notice(serialize($result));

	// 	return $result;
	// }

	/**
	 * Получаем параметры текущего портала
	 * @return [type]
	 */
	public function getPortal()
	{
		// if (!empty($this->storage->Read('portal', $this->session->domain))) {

		if (!empty($this->portalRead)) {
			// if(Portals::existsPortal($this->domain)) {
			// 	$portal = Portals::getPortal($this->domain)->toArray();

			// $portal = $this->storage->Read('portal', $this->session->domain);
			$portal = $this->portalRead;

			$this->appSettings = $portal;
			// $this->clientId = $portal['client_id'];
			// $this->clientSecret = $portal['client_secret'];
			// $this->webHookUrl = $portal['web_hook_url'];
			// $this->currentEncoding = $portal['current_encoding'];
			// $this->blockLog = $portal['block_log'];
			// $this->logsDir = $portal['logs_dir'];
			// $this->logTypeDump = $portal['log_type_dump'];
			// $this->ignoreSsl = $portal['ignore_ssl'];
			// dd($this->domain);
			$this->settings = static::getAppSettings($this->domain);
			$this->install = true;

			return true;
		} else {
			return false;
		}
	}

	/**
	 * To do - разобраться полезно ли то что отправляет сервер аутентификации...
	 * Реализовать работу через Storage
	 * @param mixed $domain
	 * @param array $arSettings
	 * @param bool $isInstall
	 * 
	 * @return [type]
	 */
	public function setAppSettings(array $arSettings, $isInstall = false)
	{
		//  То что возвращает сервер авторизации
		// 		array:11 [▼ // app/Helpers/Bitrix/ApiClient.php:111
		//   "access_token" => "14c3ff630061b4fa0057d7f600000001201c07bc1918690b8d8f0fe4556dcbad043f7e"
		//   "expires" => 1677706004
		//   "expires_in" => 3600
		//   "scope" => "app"
		//   "domain" => "oauth.bitrix.info"
		//   "server_endpoint" => "https://oauth.bitrix.info/rest/"
		//   "status" => "L"
		//   "client_endpoint" => "https://doomsdaycorp.bitrix24.ru/rest/"
		//   "member_id" => "8dda3464fc9c5b7b7896d643793368e1"
		//   "user_id" => 1
		//   "refresh_token" => "044227640061b4fa0057d7f600000001201c07a36ca00afcc03d6576d15878e8b69799"
		// ]
		// КОстыль, пока не понял нужен ли он тут вообще
		// if (isset($arSettings['server_endpoint'])) {
		// 	$parm = [
		// 		'access_token' => $arSettings['access_token'],
		// 		'expires_in' => $arSettings['expires_in'],
		// 		'refresh_token' => $arSettings['refresh_token'],
		// 	];
		// } else {
		// 	$parm = [
		// 		'access_token' => $arSettings['access_token'],
		// 		'expires_in' => $arSettings['expires_in'],
		// 		'application_token' => $arSettings['application_token'],
		// 		'refresh_token' => $arSettings['refresh_token'],
		// 		'domain' => $arSettings['domain'],
		// 		'client_endpoint' => $arSettings['client_endpoint'],
		// 	];
		// }

		// dd( $arSettings);
		$return = false;
		// $settings = static::getAppSettings($domain);

		if ($this->portal) {
			$settings = $this->settings;
		} else {
			$settings = $this->getAppSettings($this->domain);
		}

		if ($arSettings['domain'] == "oauth.bitrix.info") {
			$settings = $this->getAppSettings($this->domain);
		}
		// $settings = $this->getAppSettings($this->domain);

		if ($isInstall == true && $settings) {
			// $return = PortalsSettings::where('domain', $domain)->first()->update($parm);
			 $this->storage->Update('setting', $this->domain, $arSettings);
			 $this->settings = array_merge($this->settings, $arSettings);
			 $return = true;
		} else {
			// $return = PortalsSettings::insert($parm);
			$this->storage->Create('setting', $arSettings);
			$this->settings = $arSettings;
			$return = true;
		}
		return $return;
	}

	/**
	 * Устанавливаем параметры портала
	 * @param mixed $domain
	 * @param array $arSettings
	 * 
	 * @return [type]
	 */
	public function setPortal($domain, array $arSettings)
	{
		// $parm = [
		// 	'domain' => $arSettings['domain'],
		// 	'web_hook_url' => $arSettings['webHookUrl'],
		// 	'client_id' => $arSettings['clientId'],
		// 	'client_secret' => $arSettings['clientSecret'],
		// 	'current_encoding' => $arSettings['currentEncoding'],
		// 	'block_log' => $arSettings['blockLog'],
		// 	//  НЕ задаем т.к. не используем лог..
		// 	// 'logs_dir' => $arSettings['logsDir'],
		// 	'log_type_dump' => $arSettings['logTypeDump'],
		// 	'ignore_ssl' => $arSettings['ignoreSsl'],
		// ];
		$return = false;

		// if (Portals::existsPortal($domain)) {
		// if (!empty($this->storage->Read('portal', $this->session->domain))) {
		if (!empty($this->portalRead)) {
			// $return = Portals::where('domain', $domain)->update($parm);
			$return = $this->storage->Update('portal', $domain, $arSettings);
		} else {
			// dd($arSettings,$this->storage->Create('portal', $arSettings));
			// $return = Portals::insert($parm);
			$return = $this->storage->Create('portal', $arSettings);
		}
		return $return;
	}

	/**
	 * To do - добавить работу со Storage, перенести установку БД или json в пользовательский код.
	 * @param mixed $domain
	 * 
	 * @return [type]
	 */
	public function getAppSettings($domain)
	{
		// if (PortalsSettings::where('domain', '=', $domain)->exists() && Portals::existsPortal($domain)) {
		// if (!empty($this->storage->Read('setting', $this->session->domain)) && !empty($this->storage->Read('portal', $this->session->domain))) {
		if (!empty($this->portalRead) && !empty($this->settingRead)) {

			// $appsetting = PortalsSettings::where('domain', $domain)->first()->toArray();
			// $appsetting = $this->storage->Read('setting', $this->session->domain);
			$appsetting  = $this->settingRead;

			// $portal = Portals::select("client_id", 'client_secret')->where('domain', $domain)->first()->toArray();
			// $portal = $this->storage->Read('portal', $this->session->domain);
			$portal = $this->portalRead;
			$appsetting['C_REST_CLIENT_ID'] = $portal['client_id'];
			$appsetting['C_REST_CLIENT_SECRET'] = $portal['client_secret'];

			// Добавить проверку
			// if(
			// 	!empty($arData[ 'access_token' ]) &&
			// 	!empty($arData[ 'domain' ]) &&
			// 	!empty($arData[ 'refresh_token' ]) &&
			// 	!empty($arData[ 'application_token' ]) &&
			// 	!empty($arData[ 'client_endpoint' ])
			// )
			// {
			// 	$isCurrData = true;
			// }

			return $appsetting;
		} else {
			return false;
		}
	}

	public function Call($method, $params = [])
	{
		$arPost = [
			'method' => $method,
			'params' => $params
		];

		$result = $this->callCurl($arPost);
		return $result;
	}

	/**
	 * To do - Добавить логгирование
	 * @param mixed $arParams
	 * 
	 * @return [type]
	 */
	protected function callCurl($arParams)
	{
		// dd($arParams['params']);
		if (!function_exists('curl_init')) {
			return [
				'error'             => 'error_php_lib_curl',
				'error_information' => 'need install curl lib'
			];
		}
		// $arSettings = static::getAppSettings($this->domain);
		// dd($this,$this->settings, $this->getAppSettings($this->domain));
		// тянем параметры из объекта
		if ($this->portal) {
			$arSettings = $this->settings;
		} else {
			$arSettings = $this->getAppSettings($this->domain);
		}


		// Если добавить к параметрам auth_user запрос будет выполнен от пользователя запустившего скрипт
		// dd($arSettings, $arParams['params']);
		// if (isset($arParams['params']['auth_user'])) {
		// 	$arSettings['access_token'] = $arParams['params']['auth_user']['access_token'];
		// 	$arSettings['refresh_token'] = $arParams['params']['auth_user']['refresh_token'];
		// 	$arSettings['application_token'] = $arParams['params']['auth_user']['application_token'];
		// }
		//сессия
		if ($this->mode === 'user') {
			$arSettings['access_token'] = $this->session->auth_id;
			$arSettings['refresh_token'] = $this->session->refresh_id;
			$arSettings['application_token'] = $this->session->app_sid;
		}
		if ($arSettings !== false) {
			// dd(1);
			if (isset($arParams['this_auth']) && $arParams['this_auth'] == 'Y') {
				$url = 'https://oauth.bitrix.info/oauth/token/';
			} else {
				$url = $arSettings["client_endpoint"] . $arParams['method'] . '.' . static::TYPE_TRANSPORT;
				if (empty($arSettings['is_web_hook']) || $arSettings['is_web_hook'] != 'Y') {
					$arParams['params']['auth'] = $arSettings['access_token'];
				}
			}

			$sPostFields = http_build_query($arParams['params']);
			try {
				$obCurl = curl_init();
				curl_setopt($obCurl, CURLOPT_URL, $url);
				curl_setopt($obCurl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($obCurl, CURLOPT_POSTREDIR, 10);
				curl_setopt($obCurl, CURLOPT_USERAGENT, 'Bitrix24 CRest PHP ' . static::VERSION);
				if ($sPostFields) {
					curl_setopt($obCurl, CURLOPT_POST, true);
					curl_setopt($obCurl, CURLOPT_POSTFIELDS, $sPostFields);
				}
				curl_setopt(
					$obCurl,
					CURLOPT_FOLLOWLOCATION,
					(isset($arParams['followlocation']))
						? $arParams['followlocation'] : 1
				);
				// dd($this->appSetting);
				if ($this->appSettings['ignore_ssl'] === true) {
					curl_setopt($obCurl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($obCurl, CURLOPT_SSL_VERIFYHOST, false);
				}
				$out = curl_exec($obCurl);
				$info = curl_getinfo($obCurl);
				if (curl_errno($obCurl)) {
					$info['curl_error'] = curl_error($obCurl);
				}
				if (static::TYPE_TRANSPORT == 'xml' && (!isset($arParams['this_auth']) || $arParams['this_auth'] != 'Y')) //auth only json support
				{
					$result = $out;
				} else {
					$result = self::expandData($out);
				}
				curl_close($obCurl);
				if (!empty($result['error'])) {
					// $result = self::GetNewAuth($arParams);
					if ($result['error'] == 'expired_token' && empty($arParams['this_auth'])) {
						$result = self::GetNewAuth($arParams);
					} else {
						// dd(2);
						$arErrorInform = [
							'expired_token'          => 'expired token, cant get new auth? Check access oauth server.',
							'invalid_token'          => 'invalid token, need reinstall application',
							'invalid_grant'          => 'invalid grant, check out define C_REST_CLIENT_SECRET or C_REST_CLIENT_ID',
							'invalid_client'         => 'invalid client, check out define C_REST_CLIENT_SECRET or C_REST_CLIENT_ID',
							'QUERY_LIMIT_EXCEEDED'   => 'Too many requests, maximum 2 query by second',
							'ERROR_METHOD_NOT_FOUND' => 'Method not found! You can see the permissions of the application: CRest::call(\'scope\')',
							'NO_AUTH_FOUND'          => 'Some setup error b24, check in table "b_module_to_module" event "OnRestCheckAuth"',
							'INTERNAL_SERVER_ERROR'  => 'Server down, try later'
						];
						if (!empty($arErrorInform[$result['error']])) {
							$result['error_information'] = $arErrorInform[$result['error']];
						}
					}
				}
				if (!empty($info['curl_error'])) {
					$result['error'] = 'curl_error';
					$result['error_information'] = $info['curl_error'];
				}

				// static::setLog(
				// 	[
				// 		'url'    => $url,
				// 		'info'   => $info,
				// 		'params' => $arParams,
				// 		'result' => $result
				// 	],
				// 	'callCurl'
				// );

				return $result;
			} catch (\Exception $e) {
				// static::setLog(
				// 	[
				// 		'message' => $e->getMessage(),
				// 		'code' => $e->getCode(),
				// 		'trace' => $e->getTrace(),
				// 		'params' => $arParams
				// 	],
				// 	'exceptionCurl'
				// );

				return [
					'error' => 'exception',
					'error_exception_code' => $e->getCode(),
					'error_information' => $e->getMessage(),
				];
			}
		} else {
			// static::setLog(
			// 	[
			// 		'params' => $arParams
			// 	],
			// 	'emptySetting'
			// );
		}

		return [
			'error'             => 'no_install_app',
			'error_information' => 'error install app, pls install local application '
		];
	}

	protected function GetNewAuth($arParams)
	{
		$result = [];
		// $arSettings = $this->getAppSettings($this->domain);
		//для тестирования сессий
		if ($this->portal) {
			$arSettings = $this->settings;
		} else {
			$arSettings = $this->getAppSettings($this->domain);
		}

		if ($arSettings !== false) {
			$arParamsAuth = [
				'this_auth' => 'Y',
				'params'    =>
				[
					'client_id'     => $arSettings['C_REST_CLIENT_ID'],
					'grant_type'    => 'refresh_token',
					'client_secret' => $arSettings['C_REST_CLIENT_SECRET'],
					'refresh_token' => $arSettings["refresh_token"],
				]
			];
			$newData = static::callCurl($arParamsAuth);
			if (isset($newData['C_REST_CLIENT_ID'])) {
				unset($newData['C_REST_CLIENT_ID']);
			}
			if (isset($newData['C_REST_CLIENT_SECRET'])) {
				unset($newData['C_REST_CLIENT_SECRET']);
			}
			if (isset($newData['error'])) {
				unset($newData['error']);
			}

			// if (static::setAppSettings($this->domain, $newData, true)) {
			$auth = $this->setAppSettings($newData, true);
			// if ($this->setAppSettings($newData, true)) {
			if ($auth) {
				$arParams['this_auth'] = 'N';
				$result = static::callCurl($arParams);
				// dd($newData,$auth, $arParams, $result);
			}

			// dd($result);
		}
		return $result;
	}

	protected function expandData($data)
	{
		$return = json_decode($data, true);
		if ($this->appSettings['current_encoding']) {
			$return = static::changeEncoding($return, false);
		}
		return $return;
	}

	protected function changeEncoding($data, $encoding = true)
	{
		if (is_array($data)) {
			$result = [];
			foreach ($data as $k => $item) {
				$k = self::changeEncoding($k, $encoding);
				$result[$k] = self::changeEncoding($item, $encoding);
			}
		} else {
			if ($encoding) {
				$result = iconv($this->appSettings['current_encoding'], "UTF-8//TRANSLIT", $data);
			} else {
				$result = iconv("UTF-8", $this->appSettings['current_encoding'], $data);
			}
		}

		return $result;
	}

	public function isInstall()
	{
		return $this->install;
	}

	public function __get($property)
	{
		return $this->$property; // просто вернем свойство
	}
}
