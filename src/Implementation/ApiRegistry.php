<?php

namespace Bitrix24RestSdk\Implementation;


class ApiRegistry
{
    private static array $services = [];

    public static function set(string $key, $value)
    {
        self::$services[$key] = $value;
    }

    public static function get(string $key)
    {
        if (!isset(self::$services[$key])) {
            throw new \Exception('Invalid key given');
        }
        return self::$services[$key];
    }
}
