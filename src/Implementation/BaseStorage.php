<?php

namespace Bitrix24RestSdk\Implementation;

use Bitrix24RestSdk\Core\Interfaces\StorageInterface;

class BaseStorage implements StorageInterface
{
    public function Create(string $entity, array $data)
    {
        return true;
    }

    public function Update(string $entity, $id, array $data)
    {
        return true;
    }

    public function Read(string $entity,$id)
    {
        return true;
    }

    public function Delete(string $entity,$id)
    {
        return true;
    }
}
