<?php

namespace Bitrix24RestSdk\Implementation;

use Bitrix24RestSdk\Core\Interfaces\StorageInterface;
use Bitrix24RestSdk\Core\Interfaces\SessionInterface;

class SessionBx24 implements SessionInterface
{
    protected static self|null $instance = null;

    public $domain;
    public $protocol;
    public $lang;
    public $app_sid;
    public $auth_id;
    public $auth_expires;
    public $refresh_id;
    public $member_id;
    public $status;
    public $placement;
    public $placement_options;

    /**
     * @param array $parm
     * array $parm = [] - устаревший метод
     */
    public function __construct(array $parm = [])
    {
        $this->domain = (isset($parm['DOMAIN'])) ? $parm['DOMAIN'] : null;
        $this->protocol = (isset($parm['PROTOCOL'])) ? $parm['PROTOCOL'] : null;
        $this->lang = (isset($parm['LANG'])) ? $parm['LANG'] : null;
        $this->app_sid = (isset($parm['APP_SID'])) ? $parm['APP_SID'] : null;
        $this->auth_id = (isset($parm['AUTH_ID'])) ? $parm['AUTH_ID'] : null;
        $this->auth_expires = (isset($parm['AUTH_EXPIRES'])) ? $parm['AUTH_EXPIRES'] : null;
        $this->refresh_id = (isset($parm['REFRESH_ID'])) ? $parm['REFRESH_ID'] : null;
        $this->member_id = (isset($parm['member_id'])) ? $parm['member_id'] : null;
        $this->status = (isset($parm['status'])) ? $parm['status'] : null;
        $this->placement = (isset($parm['PLACEMENT'])) ? $parm['PLACEMENT'] : null;
        $this->placement_options = (isset($parm['PLACEMENT_OPTIONS'])) ? $parm['PLACEMENT_OPTIONS'] : null;
    }

    public function Start()
    {
        $this->Request();
        return true;
    }
    public function Custom(array $parm)
    {
        $this->__construct($parm);
        return true;
    }

    public function Request()
    {
        // dd($_REQUEST);
        $this->__construct($_REQUEST);
        // $this->domain = $_REQUEST['DOMAIN'];
        // $this->protocol = $_REQUEST['PROTOCOL'];
        // $this->lang = $_REQUEST['LANG'];
        // $this->app_sid = $_REQUEST['APP_SID'];
        // $this->auth_id = $_REQUEST['AUTH_ID'];
        // $this->auth_expires = $_REQUEST['AUTH_EXPIRES'];
        // $this->refresh_id = $_REQUEST['REFRESH_ID'];
        // $this->member_id = $_REQUEST['member_id'];
        // $this->status = $_REQUEST['status'];
        // $this->placement = $_REQUEST['PLACEMENT'];
        // $this->placement_options = (isset($_REQUEST['PLACEMENT_OPTIONS'])) ? $_REQUEST['PLACEMENT_OPTIONS'] : null;
        return true;
    }
    public function Storage(StorageInterface $storage, $id)
    {
        $session = $storage->Read('setting', $id);
        // dd($session);
        $parm = [
            'DOMAIN' =>  $session['domain'],
            'APP_SID' => $session['application_token'],
            'AUTH_ID' => $session['access_token'],
            'AUTH_EXPIRES' => $session['expires_in'],
            'REFRESH_ID' => $session['refresh_token'],
        ];
        $this->__construct($parm);
    }


    // public static function getInstance(): static
    // {
    //     if (self::$instance === null) {
    //         self::$instance = new self;
    //     }
    //     return self::$instance;
    // }
    // public function Start()
    // {
    //     $data = new BaseStorage();
    //     if (Portals::existsPortal($this->domain)) {
    //         $portal = new ApiClient($this, $data);
    //         $portal->getPortal();
    //         $this->portal = $portal;
    //         return true;
    //     } else {
    //         $portal = new ApiClient($this, $data);
    //         $this->portal = $portal;
    //         return false;
    //     }
    // }
}
