<?php

namespace Bitrix24RestSdk\Core;

use Bitrix24RestSdk\Core\Interfaces\ApiInterface;

class RequestProcessor
{
    protected ApiInterface $api;
    protected $apiPath;
    // public $entityName;

    public function __construct(ApiInterface $api, $apiPath, /*$entityName*/)
    {
        $this->api = $api;
        $this->apiPath = $apiPath;
        // $this->entityName = $entityName;
    }

    public function Call($method, $parm = null, /*$entity = null*/)
    {
        if (isset($parm['ID']) && is_array($parm['ID'])) {
            $response = [];
            foreach ($parm['ID'] as $id) {
                $parm['ID'] = $id;
                $response[] = $this->api->Call($this->CreateMethod($method), $parm);
            }
        } else {
            $response = $this->api->Call($this->CreateMethod($method), $parm);
        }
        return $response;
    }
    protected function CreateMethod($method): string
    {
        preg_match_all('/[A-Z][^A-Z]*?/Usu',$method,$res);
        return $this->apiPath . "." . strtolower(implode('.',$res[0]));
    }
}
