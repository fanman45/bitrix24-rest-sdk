<?php

namespace Bitrix24RestSdk\Impv2;

use Bitrix24RestSdk\Core\Contracts\Storage;
use Bitrix24RestSdk\Core\Interfaces\ApiInterface;
use Bitrix24RestSdk\Core\Interfaces\StorageInterface;
use Bitrix24RestSdk\Core\Interfaces\SessionInterface;

/**
 * Адаптация для использования с Laravel v2
 *  @version 1.36
 */
class ApiClientv2 implements ApiInterface
{

	const VERSION = '1.36';
	const BATCH_COUNT    = 50; //count batch 1 query
	const TYPE_TRANSPORT = 'json'; // json or xml

	public string|null $domain;
	protected string $mode;
	// protected  string $clientId;
	// protected  string $clientSecret;
	// protected  $webHookUrl;
	// protected  string $currentEncoding;
	// protected  bool $blockLog;
	// protected  $logsDir;
	// protected  bool $logTypeDump;
	// protected  bool $ignoreSsl;
	protected  bool $install;
	//
	protected $portal;
	protected $appSettings;
	protected $settings;
	protected StorageInterface $storage;
	protected SessionInterface $session;
	protected $portalRead;
	protected $settingRead;


	public function __construct(SessionInterface $session, StorageInterface $storage)
	{
	}

	public function installApp()
	{
	}

	public function setAppSettings(array $arSettings, $isInstall = false)
	{
	}

	public function getAppSettings($domain)
	{
	}

	public function getSettingData()
	{
	}

	public function setSettingData()
	{
	}

	public function Call($method, $params = [])
	{
	}

	protected function callCurl($arParams)
	{
	}

	protected function GetNewAuth($arParams)
	{
	}

	protected function expandData($data)
	{
	}
}