<?php

namespace Bitrix24RestSdk\Core\Contracts;

interface StorageInterface
{
    public function Set(string $name, mixed $value);
    public function Get(string $name): mixed;
}
