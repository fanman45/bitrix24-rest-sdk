<?php

namespace Bitrix24RestSdk\Core\Contracts;

interface LoggerInterface
{
    public function Write(string $message);
}
