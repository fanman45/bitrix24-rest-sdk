<?php

namespace Bitrix24RestSdk\Core\Contracts\Api;

interface CoreInterface
{
    public function Call(string $method, array $parm = []);
}
