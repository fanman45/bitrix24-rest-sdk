<?php

namespace Bitrix24RestSdk\Core\Contracts\Api;

interface SessionInterface
{
    public function Set(string $name, mixed $value);
    public function Get(string $name): mixed;
}
