<?php

namespace Bitrix24RestSdk\Core\Contracts\Api;

interface ApiClientInterface
{
    public function Call(string $method, array $parm = []);
}
