<?php

namespace Bitrix24RestSdk\Core\Handlers;

use Bitrix24RestSdk\Entity\AbstractEntity;

class UfReplacer{

    protected $entityId;
    protected $entityTypeId;


    public function __construct(AbstractEntity $entity)
    {
        
    }

    public function ReplaceIB($entityService, $id, $uf) {
        // Для реплейса УС Берем сущность смотритм поля ищем Fields > ufCrm if type ib iblock_element > settings > IBLOCK_ID идем в Ulist() > ElementGet(IBLOCK_ID) тянем ID и NAME
        $item = $entityService->Fields($id);
        $res = [];
        // dd($item['fields'],$uf,$item['fields'][$uf]);
        $field = $item['fields'][$uf];
            if ($field['type'] == 'iblock_element') {
                $field['upperName'] = [lcfirst(str_replace('_', '', ucwords(strtolower($field['upperName']), '_')))];
                dd($field['upperName']);
                if($field['upperName'] == $uf){
                    // dd($field);
                $res[$field['upperName']]['title'] = $field['title'];
                $ulists = new ListsService(static::$api);
                $props = $ulists->ElementGet($field['settings']['IBLOCK_ID']);
                foreach ($props->raw as $prop) {
                    $res[$field['upperName']]['item'][$prop['ID']] = $prop['NAME'];
                }
            }
        }
        // dd($res);
        return $res;
    }
}
