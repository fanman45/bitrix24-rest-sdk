<?php

declare(strict_types=1);

namespace Bitrix24RestSdk\Core\Handlers;

use Bitrix24RestSdk\Core\Handlers\Replacer;
use Bitrix24RestSdk\Core\Handlers\UfReplacer;
use Bitrix24RestSdk\Entity2\AbstractEntity;
use Closure;

class Property
{
    protected $name;
    protected $type;
    protected $title;
    protected $value;
    protected $valueRaw;
    protected $parm;
    // protected $raw;
    // protected $replace;

    const TYPE = [
        'string' => true,
        'integer' => true,
        'double' => true,
        'datetime' => true,
        'date' => true,
        'user' => true,
        'crm' => '?',
        'crm_status' => true,
        'crm_currency' => true,
        'crm_lead' => true,
        'crm_category' => true,
        'crm_company' => true,
        'crm_contact' => true,
        'crm_entity' => true,
        'crm_quote' => '?',
        'enumeration' => true,
        'location' => '?',
        'address' => true,
        'money' => true,
        'file' => true,
        'employee' => true,
        'resourcebooking',
        'rest_',
        'iblock_element' => true,
        'iblock_section' => true,
        'product_property'
    ];

    /**
     * Переделать на ленивую загрузку
     * @param AbstractEntity $entity
     * @param mixed $name
     * @param bool $replace
     */
    public function __construct(AbstractEntity $entity, $name, $replace = true)
    {
        $parm = [
            'entity' => $entity,
            'field' => $entity->fieldsType[$name],
            'name' => $name,
        ];
        $this->parm = $parm;

        $field = $parm['field'];
        $this->type = $field['type'];
        $this->name = $name;
        $this->valueRaw = $entity->data[$name];

        if (isset($field['formLabel'])) {
            $this->title = $field['formLabel'];
        } else {
            $this->title = $field['title'];
        }
        // if ($replace) {
        // $this->value =
        // $this->value = match ($this->type) {
        //     'integer' =>  $this->IntegerType($parm),
        //     'double' => $this->DoubleType($parm),
        //     'string' => $this->StringType($parm),
        //     'char' => $this->CharType($parm),
        //     'date' => $this->DateType($parm),
        //     'datetime' => $this->DateTimeType($parm),
        //     'crm' => $this->CrmType($parm),
        //     'user' => $this->UserType($parm),
        //     'url' => $this->UrlType($parm),
        //     'crm_status' => $this->CrmStatusType($parm),
        //     'crm_currency' => $this->CrmCurrencyType($parm),
        //     'crm_lead', => $this->CrmLeadType($parm),
        //     'crm_category' => $this->CrmCategoryType($parm),
        //     'crm_company' => $this->CrmCompanyType($parm),
        //     'crm_contact' => $this->CrmContactType($parm),
        //     'crm_entity' => $this->CrmEntityType($parm),
        //     'crm_quote' => $this->CrmEntityType($parm),
        //     'enumeration' => $this->EnumerationType($parm),
        //     'location' => $this->LocationType($parm),
        //     'address' => $this->AddressType($parm),
        //     'money' => $this->MoneyType($parm),
        //     'file' => $this->FileType($parm),
        //     'employee' => $this->EmployeeType($parm),
        //     'iblock_element' => $this->IblockEelementType($parm),
        //     'iblock_section' => $this->IblockSectionType($parm),
        //     'resourcebooking' => $this->ResourceBookingType($parm),
        //     default => $this->DefaultType($parm)
        // };
        // dd($this->value);
        // }
    }

    /**
     * Нужно добавть ограничения
     * @param mixed $name
     * 
     * @return [type]
     */
    public function __get($name)
    {
        if ($name === 'value' && empty($this->$name)) {
            $this->GetValue();
        }
        return $this->$name;
    }

    /**
     * @todo добавить возможность изменения поля в зависимости от прав пользователя
     * @param mixed $name
     * @param mixed $value
     * 
     * @return [type]
     */
    public function __set($name, $value)
    {
        throw new \Exception('Свойство ' . $name . ' нельзя установить таким способом - отсутствует реализация!');
    }
    protected function GetValue()
    {
        $parm = $this->parm;
        if (!$this->IsNull($parm)) {
            $this->value = match ($this->type) {
                'integer' =>  $this->IntegerType($parm),
                'double' => $this->DoubleType($parm),
                'string' => $this->StringType($parm),
                'char' => $this->CharType($parm),
                'date' => $this->DateType($parm),
                'datetime' => $this->DateTimeType($parm),
                'crm' => $this->CrmType($parm),
                'user' => $this->UserType($parm),
                'url' => $this->UrlType($parm),
                'crm_status' => $this->CrmStatusType($parm),
                'crm_currency' => $this->CrmCurrencyType($parm),
                'crm_lead', => $this->CrmLeadType($parm),
                'crm_category' => $this->CrmCategoryType($parm),
                'crm_company' => $this->CrmCompanyType($parm),
                'crm_contact' => $this->CrmContactType($parm),
                'crm_entity' => $this->CrmEntityType($parm),
                'crm_quote' => $this->CrmEntityType($parm),
                'enumeration' => $this->EnumerationType($parm),
                'location' => $this->LocationType($parm),
                'address' => $this->AddressType($parm),
                'money' => $this->MoneyType($parm),
                'file' => $this->FileType($parm),
                'employee' => $this->EmployeeType($parm),
                'iblock_element' => $this->IblockEelementType($parm),
                'iblock_section' => $this->IblockSectionType($parm),
                'resourcebooking' => $this->ResourceBookingType($parm),
                'product_property' => $this->ProductPropertyType($parm),
                default => $this->DefaultType($parm)
            };
        } else {
            $this->value = null;
        }
    }
    protected function IsNull($parm)
    {
        return ($parm['entity']->data[$parm['name']] == null) ? true  : false;
    }

    protected function EntityReplaser($parm, Closure $func = null)
    {
        if (\is_null($func)) {
            $func = function ($id) {
                return $id;
            };
        }
        if ($parm['field']['isMultiple']) {
            $res = [];
            foreach ($parm['entity']->data[$parm['name']] as $item) {
                $res[] = $func($item);
            }
            return (object) $res;
        } else {
            return $func($parm['entity']->data[$parm['name']]);
        }
    }

    protected function DefaultType($parm)
    {
        return $parm['entity']->data[$parm['name']];
    }

    protected function IntegerType($parm)
    {
        return (int) $parm['entity']->data[$parm['name']];
    }

    protected function DoubleType($parm)
    {
        return (float) $parm['entity']->data[$parm['name']];
    }

    protected function StringType($parm)
    {
        // return (string) $parm['entity']->data[$parm['name']];

        $replaseTo = function ($item) use ($parm) {
            return (string) $item;
        };
        return $this->EntityReplaser($parm, $replaseTo);
    }

    protected function CharType($parm)
    {
        return ($parm['entity']->data[$parm['name']] === 'Y') ? true : false;
    }

    protected function DateType($parm)
    {
        return new \DateTime($parm['entity']->data[$parm['name']]);
    }

    protected function DateTimeType($parm)
    {
        return new \DateTime($parm['entity']->data[$parm['name']]);
    }

    /**
     * @todo Добавить обработку типов сущностей
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function CrmType($parm)
    {
        // dd($parm['entity']->data[$parm['name']]);
        $replaseTo = function ($id) use ($parm) {

            // dd($parm['field']['settings']);

            foreach ($parm['field']['settings'] as $key => $value) {
                // dd(stripos($key,'DYNAMIC'),$key);
                if (stripos('DYNAMIC', $key) !== false) {
                    // dd($id);
                    $ar = explode('_', $key);
                    $entityTypeId = $ar[1];
                    // return $parm['entity']->api->SMItem()->Get((int) $entityTypeId, (int) $id)['item'];
                    return $parm['entity']->eapi->CRMEntity((int)$entityTypeId)->Get((int)$id);
                    // dd($entityTypeId);
                }
                if (isset($parm['field']['settings']['COMPANY']) && ($parm['field']['settings']['COMPANY'] == 'Y')) {
                    return $parm['entity']->eapi->Company()->Get($id);
                }
                if (isset($parm['field']['settings']['DEAL']) && ($parm['field']['settings']['DEAL'] == 'Y')) {
                    return $parm['entity']->eapi->Deal()->Get($id);
                }
            }

            // if (isset($parm['field']['settings']['COMPANY']) && ($parm['field']['settings']['COMPANY'] == 'Y')) {
            //     return $parm['entity']->api->Company()->Get($id);
            // } elseif (isset($parm['field']['settings']['LEAD']) && ($parm['field']['settings']['LEAD'] == 'Y')) {
            //     return $parm['entity']->api->Lead()->Get($id);
            // }
        };
        // if($field['settings']['COMPANY'] == 'Y'){
        //     return $entity->data[$name];
        // }elseif($field['settings']['LEAD']== 'Y'){
        //     return $entity->data[$name];
        // }else{
        //     return $entity->data[$name];
        // }    
        // $replaseTo = function ($id) use ($parm) {
        //    return  match($parm['field']['settings']['LEAD']);
        //DYNAMIC_171
        //LEAD
        // };

        return  $this->EntityReplaser($parm, $replaseTo);
    }
    protected function UserType($parm)
    {
        // $replaseTo = function ($id) use ($parm) {
        //     return $parm['entity']->api->User()->Get($id);
        // };

        $replaseTo = function ($id) use ($parm) {

            // dd($parm['entity']->eapi,$parm['entity']->eapi->User()->Get($id));
            // dd($parm['entity']->eapi->User()->Get($id),$id);
            // dd($parm['entity']->eapi->User()->Get($id));
            return $parm['entity']->eapi->User()->Get($id);
        };
        return $this->EntityReplaser($parm, $replaseTo);
    }

    /**
     * @todo Нужно подумать как лучше обрабатывать http и https
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function UrlType($parm)
    {
        return 'https://' . (string) $parm['entity']->data[$parm['name']];
    }
    /**
     * @todo добавить поиск статуса в параметрах метода List
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function CrmStatusType($parm)
    {
        $value = $parm['entity']->data[$parm['name']];
        $statusList = $parm['entity']->api->Status()->List();
        $res = [];
        foreach ($statusList as $status) {
            if ($status['STATUS_ID'] == $value) {
                $res = $status;
            }
        }
        return (object) $res;
    }
    protected function CrmCurrencyType($parm)
    {
        return (string) $parm['entity']->data[$parm['name']];
    }

    protected function CrmLeadType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            return $parm['entity']->api->Lead()->Get((int)$id);
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    protected function CrmCategoryType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            return $parm['entity']->api->Category()->Get($parm['entity']->entityTypeId, (int) $id)['category'];
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    protected function CrmCompanyType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            return $parm['entity']->eapi->Company()->Get((int) $id);
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    protected function CrmContactType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            return $parm['entity']->eapi->Contact()->Get((int) $id);
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    /**
     * @todo Возможно есть множественный вариант - добавить обработку
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function CrmEntityType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            return $parm['entity']->api->SMItem()->Get($parm['field']['settings']['parentEntityTypeId'], (int) $id)['item'];
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }
    /**
     * @todo Необходимо использовать специльный метод crm.quote
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function CrmQuiteType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            return $parm['entity']->api->SMItem()->Get($parm['field']['settings']['parentEntityTypeId'], (int) $id);
        };
        return $this->EntityReplaser($parm, $replaseTo);
    }

    protected function EnumerationType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            $enumItems = $parm['field']['items'];
            $res = '';
            foreach ($enumItems as $item) {
                if ($item['ID'] == $id) {
                    $res = $item['VALUE'];
                }
                //  else {
                //     // $res = $parm['field']['settings']['CAPTION_NO_VALUE'];
                // }
            }
            return $res;
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    protected function ProductPropertyType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            $enumItems = $parm['field']['values'];
            $res = '';
            foreach ($enumItems as $item) {
                if ($item['ID'] == $id['value']) {
                    $res = $item['VALUE'];
                }
            }
            return $res;
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    /**
     * @todo Для реализации необходимо протестировать на портале с включенными местоположением
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function LocationType($parm)
    {
        return $parm['entity']->data[$parm['name']];
    }

    protected function AddressType($parm)
    {
        $parse = explode('|', $parm['entity']->data[$parm['name']]);
        $geo = explode(';', $parse[1]);

        // $result[] = (object) [
        //     'text' => $parse[0],
        //     'geo' => (object) ['lat' => $geo[0], 'lon' => $geo[1]],
        //     'code' => $parse[2]
        // ];

        $result['text'] = isset($parse[0]) ? $parse[0] : "";
        $result['geo'] = (isset($geo[0]) && isset($geo[1])) ? (object) ['lat' => $geo[0], 'lon' => $geo[1]]  : "";
        $result['code'] = isset($parse[2]) ? $parse[2] : "";

        return (object) $result;
    }
    /**
     * @todo Добавить обработку значения "по умолчанию"
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function MoneyType($parm)
    {
        if ($parm['entity']->data[$parm['name']] != "") {
            $parse = explode('|', $parm['entity']->data[$parm['name']]);
            return (object) ['sum' => $parse[0], 'currency' => $parse[1]];
        } else {
            return $parm['entity']->data[$parm['name']];
        }
    }

    /**
     * @todo Добавить обработку параметров
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function FileType($parm)
    {
        $replaseTo = function ($id) use ($parm) {

            return (object) $id;
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    /**
     * Возвращает юзеров
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function EmployeeType($parm)
    {
        return $this->UserType($parm);
    }

    /**
     * @todo Добавить обработку пагинации, переделать на точечный поиск по фильтрам, возможно сделать реализацию на списках
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function IblockEelementType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            $ibItemList = $parm['entity']->api->Lists()->ElementGet($parm['field']['settings']['IBLOCK_ID']);
            foreach ($ibItemList->raw as $item) {
                if ($item['ID'] == $id) {
                    $res = $item['NAME'];
                }
            }
            return $res;
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    /**
     * Реализация на api универсальных списков
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function IblockSectionType($parm)
    {
        $replaseTo = function ($id) use ($parm) {
            $ibItemList = $parm['entity']->api->Lists()->SectionGet($parm['field']['settings']['IBLOCK_ID']);
            // dd($ibItemList);
            foreach ($ibItemList->raw as $item) {
                if ($item['ID'] == $id) {
                    $res = $item['NAME'];
                }
            }
            return $res;
        };
        return  $this->EntityReplaser($parm, $replaseTo);
    }

    /**
     * Отсутствует реализация
     * @todo Непонятно откуда брать время
     * @param mixed $parm
     * 
     * @return [type]
     */
    protected function ResourceBookingType($parm)
    {
        // $replaseTo = function ($id) use ($parm) {
        //     // dd($parm['field']);
        //     return $parm['entity']->api->SMItem()->Get($parm['field']['settings']['parentEntityTypeId'], (int) $id);
        // };
        //return  $this->EntityReplaser($parm, $replaseTo);
        return  $this->EntityReplaser($parm);
    }
}
