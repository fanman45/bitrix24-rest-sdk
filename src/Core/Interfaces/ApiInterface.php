<?php

namespace Bitrix24RestSdk\Core\Interfaces;

interface ApiInterface
{
    public function __construct(SessionInterface $session, StorageInterface $storage);
    public function Call(string $method, array $parm = []);
    public function installApp();
}
