<?php

namespace Bitrix24RestSdk\Core\Interfaces;

interface StorageInterface
{
    public function Create(string $entity,array $data);
    public function Update(string $entity,$id, array $data);
    public function Read(string $entity, $id);
    public function Delete(string $entity,$id);

    // public function portalExist($name): bool;
    // public function getPortal($name): array;
    // public function updatePortal($name): bool;
    // public function createPortal($name): bool;
    // public function getSetting($name): array;
    // public function updateSetting($name): bool;
    // public function createSetting($name): bool;
}