<?php

namespace Bitrix24RestSdk\Core\Interfaces;

interface CrudInterface
{
    // const ENTITY_CLACC = "имя метода апи";
    public function Add(array $fields, array $parm = []);
    public function Get($id);
    public function Update($id, array $fields, array $parm = []);
    public function Delete($id);
}