<?php

namespace Bitrix24RestSdk\Core\Interfaces;

interface SessionInterface
{
    public function Start();
}
