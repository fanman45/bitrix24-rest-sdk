<?php

namespace Bitrix24RestSdk\Core\Interfaces;

interface UserInterface
{
    public function isAdmin();
    public function isUser();
    public function fullName();
}