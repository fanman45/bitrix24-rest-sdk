<?php

namespace Bitrix24RestSdk\Core;

use Bitrix24RestSdk\Core\Traits\NamespaceCreator;

class ResponseProcessor
{
    use NamespaceCreator;

    public $result;
    public $time;
    public $error;

    public function __construct($response)
    {
        if (!isset($response['error'])) {
            $this->result = $response['result'];
            $this->time =  $response['time'];
        } else {
            $this->error = [
                // $response['error_description'],
                // $response['error']
                $response
            ];
        }

        // dd($response);
        // $request;
        // $this->Processing();

    }

    public function Processing()
    {
        if ($this->error != null) {
            // if ($entity != null) {
            //     return new $entity($response['result']);
            // } else {
            //     // ResponseLogger::Get($response);
            //     return $response['result'];
            // }
            return $this->error;
        } else {
            return $this->result;
            // return dd($response);
            // return new ErrorBitrix24($response);
            // }
        }
    }
}
