<?php

namespace Bitrix24RestSdk\Core;

use Bitrix24RestSdk\Core\ServiceProvider;
use Bitrix24RestSdk\Core\Traits\Entitys;
use Bitrix24RestSdk\Entity2\Deal;
use Bitrix24RestSdk\Entity2\User;
use Bitrix24RestSdk\Entity2\CRMEntity;
use Bitrix24RestSdk\Entity2\Product;
use Bitrix24RestSdk\Entity2\Company;
use Bitrix24RestSdk\Entity2\Contact;
use Closure;

class EntityProvider
{
    use Entitys;

    protected $api;
    protected array $fieldsType;
    public array $entity;

    public function __construct(ServiceProvider $api)
    {
        $this->api = $api;
    }

    protected function initFieldsType(Closure $fields, $entityName)
    {
        if (empty($this->fieldsType[$entityName])) {
            $this->fieldsType[$entityName] = $fields();
        }
    }

    protected function initEntity(Closure $entity, $entityName)
    {
        if (empty($this->entity[$entityName])) {
            $this->entity[$entityName] = $entity();
        }
    }

    public function Deal($propValMap = [])
    {
        $entity = function () use ($propValMap) {
            return new Deal(api: $this->api, eapi: $this, fieldsType: $this->api->Deal()->Fields(), propValMap: $propValMap, /*data: $result*/);
        };

        // \Debugbar::startMeasure("deal");

        $this->initEntity($entity, __FUNCTION__);

        // \Debugbar::stopMeasure("deal");

        return $this->entity[__FUNCTION__];
    }

    // public function Lead($propValMap = [])
    // {

    //     $entity = function () use ($propValMap) {
    //         return new Deal(api: $this->api, fieldsType: $this->api->Deal()->Fields(), propValMap: $propValMap);
    //     };

    //     $this->initEntity($entity, __FUNCTION__);

    //     return $this->entity[__FUNCTION__];
    // }

    public function User($propValMap = [])
    {
        // dd($this->api->User()->Fields());
        $entity = function () use ($propValMap) {
            return new User(api: $this->api, eapi: $this, fieldsType: $this->api->User()->Fields(), propValMap: $propValMap, /*data: $result*/);
        };

        $this->initEntity($entity, __FUNCTION__);

        return $this->entity[__FUNCTION__];
    }

    public function Company($propValMap = [])
    {
        $entity = function () use ($propValMap) {
            return new Company(api: $this->api, eapi: $this, fieldsType: $this->api->Company()->Fields(), propValMap: $propValMap, /*data: $result*/);
        };

        $this->initEntity($entity, __FUNCTION__);

        return $this->entity[__FUNCTION__];
    }

    public function Contact($propValMap = [])
    {
        $entity = function () use ($propValMap) {
            return new Contact(api: $this->api, eapi: $this, fieldsType: $this->api->Contact()->Fields(), propValMap: $propValMap, /*data: $result*/);
        };

        $this->initEntity($entity, __FUNCTION__);

        return $this->entity[__FUNCTION__];
    }

    public function CRMEntity($id, $propValMap = [])
    {

        $entity = function () use ($propValMap, $id) {
            return new CRMEntity(api: $this->api, eapi: $this, fieldsType: $this->api->SMItem()->Fields($id)['fields'], propValMap: $propValMap, entityTypeId: $id);
        };
        // \Debugbar::startMeasure("CRM");
        $this->initEntity($entity, __FUNCTION__ . '_' . $id);

        // \Debugbar::stopMeasure("CRM");
        return $this->entity[__FUNCTION__ . '_' . $id];
    }

    public function Product($propValMap = [])
    {
        $entity = function () use ($propValMap) {
            return new Product(api: $this->api, eapi: $this, fieldsType: $this->api->Product()->Fields(), propValMap: $propValMap);
        };

        $this->initEntity($entity, __FUNCTION__);

        return $this->entity[__FUNCTION__];
    }
}
