<?php

namespace Bitrix24RestSdk\Core;

use Bitrix24RestSdk\Core\Interfaces\ApiInterface;
use Bitrix24RestSdk\Core\Interfaces\SessionInterface;
use Bitrix24RestSdk\Core\Interfaces\StorageInterface;
use Bitrix24RestSdk\Implementation\BaseStorage;
use Bitrix24RestSdk\Implementation\ApiClient;
use Bitrix24RestSdk\Core\Traits\Services;
class ServiceProvider
{
    use Services;
    
    protected SessionInterface $session;
    protected StorageInterface $storage;
    public ApiInterface $apiClient;
    // protected Logger $logger;
    protected $mode;

    public function __construct($session,$storage = BaseStorage::class,$apiClient = ApiClient::class, $mode = 'user')
    {
        $this->mode = $mode;
        $this->session = $session;
        $this->storage = new $storage();
        $this->apiClient = new $apiClient($this->session, $this->storage, $this->mode);
        // dd($this->session);
    }

    public function install(){
        // dd($_REQUEST);
        return $this->apiClient->installApp();
    }

    public function setPortal($doamin,$arSetting){
        // dd($_REQUEST);
        // return $this->apiClient->setPortal($doamin,$arSetting);
    }

    public function GetPlacement()
    {
        return (object)[
            'placement' => $this->session->placement,
            'options' => json_decode($this->session->placement_options)
        ];
    }
}
