<?php

namespace Bitrix24RestSdk\Core\Traits;

trait CrudEntity
{

    public function Add(int $entityTypeId, array $fields)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'fields' => $fields]);
    }

    public function Update(int $entityTypeId, int $id, array $fields)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id, 'fields' => $fields]);
    }

    public function Delete(int $entityTypeId, int $id)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id]);
    }

    public function Get(int $entityTypeId, int $id, $crudEntity = null)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id], $crudEntity);
    }

    public function List(int $entityTypeId)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId]);
    }
}
