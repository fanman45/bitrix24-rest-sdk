<?php

namespace Bitrix24RestSdk\Core\Traits;

use Bitrix24RestSdk\Core\RequestProcessor;
use Bitrix24RestSdk\Core\ResponseProcessor;

trait Base
{
    protected function GenMethod($func, array $parm = null, $genEntity = null,$fields = null)
    {
        $request = new RequestProcessor($this->api, $this::API_PATH, $this::ENTITY_NAME);

        $response = new ResponseProcessor($request->Call($func, $parm));
        if ($genEntity == null) {
            return $response->Processing();
        } else {

            // if($genEntity == 'Bitrix24RestSdk\Entity\Deal'){

                if($fields != null){
                    return new $genEntity($response->Processing(),$this->api,$fields);
                }else{
                    return new $genEntity($response->Processing(),$this->api);
                }

           
            // }else{
            //     return new $genEntity($response->Processing());
            // }
        }
    }
}
