<?php

namespace Bitrix24RestSdk\Core\Traits;
trait Crud
{

    public function Add(array $fields, array $parm = [])
    {
        return $this->GenMethod(__FUNCTION__, ['fields' => $fields, 'params' => $parm]);
    }

    public function Get($id)
    {
        return $this->GenMethod(__FUNCTION__, ['ID' => $id]);
    }

    public function Delete($id)
    {
        return $this->GenMethod(__FUNCTION__, ['ID' => $id]);
    }

    public function Update($id, array $fields, array $parm = [])
    {
        return $this->GenMethod(__FUNCTION__, ['ID' => $id, 'fields' => $fields, 'params' => $parm]);
    }
}
