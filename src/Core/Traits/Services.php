<?php

namespace Bitrix24RestSdk\Core\Traits;

use Bitrix24RestSdk\Services\CRM\DealService;
use Bitrix24RestSdk\Services\CRM\LeadService;
use Bitrix24RestSdk\Services\UserService;
use Bitrix24RestSdk\Services\CRM\CategoryService;
use Bitrix24RestSdk\Services\CRM\Dynamic\ItemService;
use Bitrix24RestSdk\Services\CRM\Dynamic\SettingService;
use Bitrix24RestSdk\Services\ListsService;
use Bitrix24RestSdk\Services\CRM\UserfieldService;
use Bitrix24RestSdk\Services\CRM\StatusService;
use Bitrix24RestSdk\Services\CRM\CompanyService;
use Bitrix24RestSdk\Services\CRM\ContactService;
use Bitrix24RestSdk\Services\CRM\ProductService;
use Bitrix24RestSdk\Services\EventService;
use Bitrix24RestSdk\Services\IblockService;
use Bitrix24RestSdk\Services\ImService;
use Bitrix24RestSdk\Services\UserfieldtypeService;
use Bitrix24RestSdk\Services\PlacementService;
use Bitrix24RestSdk\Services\Bizproc\WorkFlowService;

trait Services
{
    public function User(): UserService
    {
        return new UserService($this->apiClient);
    }

    public function Deal(): DealService
    {
        return new DealService($this->apiClient);
    }

    public function Lead(): LeadService
    {
        return new LeadService($this->apiClient);
    }

    public function Category(): CategoryService
    {
        return new CategoryService($this->apiClient);
    }

    public function Company(): CompanyService
    {
        return new CompanyService($this->apiClient);
    }

    public function Contact(): ContactService
    {
        return new ContactService($this->apiClient);
    }

    public function SMItem(): ItemService
    {
        return new ItemService($this->apiClient);
    }

    public function SMSetting(): SettingService
    {
        return new SettingService($this->apiClient);
    }

    public function UList(): ListsService
    {
        return new ListsService($this->apiClient);
    }

    public function Userfield(): UserfieldService
    {
        return new UserfieldService($this->apiClient);
    }

    public function Status(): StatusService
    {
        return new StatusService($this->apiClient);
    }

    public function Iblock(): IblockService
    {
        return new IblockService($this->apiClient);
    }

    public function Lists(): ListsService
    {
        return new ListsService($this->apiClient);
    }

    public function UserFieldType(): UserfieldtypeService
    {
        return new UserfieldtypeService($this->apiClient);
    }

    public function Product(): ProductService
    {
        return new ProductService($this->apiClient);
    }
    public function Im(): ImService
    {
        return new ImService($this->apiClient);
    }
    public function Event(): EventService
    {
        return new EventService($this->apiClient);
    }
    public function Placement(): PlacementService
    {
        return new PlacementService($this->apiClient);
    } 
    public function BizprocWorkFlow(): WorkFlowService
    {
        return new WorkFlowService($this->apiClient);
    } 
}
