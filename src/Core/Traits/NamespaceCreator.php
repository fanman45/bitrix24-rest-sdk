<?php

namespace Bitrix24RestSdk\Core\Traits;

trait NamespaceCreator
{

    public static function GenNameService($methodName): string
    {
        $section = explode('\\', __CLASS__);
        $a = array_pop($section);
        $b = array_pop($section);

        // костыль для рыботы с первым уровнем сервисов
        if ($b == 'Services') {
            return strtolower($a . "." . $methodName);
        } else {
            return strtolower($b . "." . $a . "." . $methodName);
        }
    }

    public static function GenNameEntity(): string
    {
        $a = explode('\\', __CLASS__);
        $b = array_pop($a);
        return "Bitrix24RestSdk\Entity\\" . $b;
    }
}
