<?php

namespace Bitrix24RestSdk\Services;

use Bitrix24RestSdk\Services\AbstractService;

class ImService extends AbstractService
{
    public const API_PATH = 'im.notify.system';
    // public const ENTITY_NAME =  UList::class;

    public function Add($userId, $message, $messageOut = null, $tag = null, $subTag = null, $attach = null)
    {
        return $this->GenMethod(__FUNCTION__, ['USER_ID' => $userId, 'MESSAGE' => $message, 'MESSAGE_OUT' => $messageOut, 'TAG' => $tag, 'SUB_TAG' => $subTag, 'ATTACH' => $attach]);
    }
    
    // public function Delete($userTypeId)
    // {
    //     return $this->GenMethod(__FUNCTION__, ['USER_TYPE_ID' => $userTypeId]);
    // }

    // public function List()
    // {
    //     return $this->GenMethod(__FUNCTION__);
    // }

    // public function Update($userTypeId, $handler, $title, $description,$options)
    // {
    //     return $this->GenMethod(__FUNCTION__, ['USER_TYPE_ID' => $userTypeId, 'HANDLER' => $handler, 'TITLE' => $title, 'DESCRIPTION' => $description, 'OPTIONS' => $options]);
    // }

}