<?php


namespace Bitrix24RestSdk\Services;

use Bitrix24RestSdk\Core\Interfaces\Service;
use Bitrix24RestSdk\Core\Interfaces\ApiInterface;
use Bitrix24RestSdk\Core\Traits\Base;

abstract class AbstractService implements Service
{
    use Base;

    protected ApiInterface $api;
    // protected RequestProcessor $request;

    public const API_PATH = 'api.method.name';
    public const ENTITY_NAME = 'class::ClassName';

    public function __construct(ApiInterface $api)
    {
        $this->api = $api;
        // $this->request = new RequestProcessor($this->session, $this::API_PATH, $this::ENTITY_NAME);
    }

    // public static function Create()
    // {
    //     $session =  SessionBx24::getInstance();
    //     return new static($session);
    // }
}
