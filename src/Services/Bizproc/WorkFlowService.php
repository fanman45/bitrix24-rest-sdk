<?php

namespace Bitrix24RestSdk\Services\Bizproc;

use Bitrix24RestSdk\Services\AbstractService;

class WorkFlowService extends AbstractService
{
    public const API_PATH = 'bizproc.workflow';

    public function Start($templateId, $documentId, $parameters = null)
    {
        return $this->GenMethod(__FUNCTION__, ['TEMPLATE_ID' => $templateId, 'DOCUMENT_ID' => $documentId, 'PARAMETERS' => $parameters]);
    }
}