<?php

namespace Bitrix24RestSdk\Services;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\Crud;
use Bitrix24RestSdk\Core\Interfaces\CrudInterface;
use Bitrix24RestSdk\Entity\UList;

class ListsService extends AbstractService
{
    public const API_PATH = 'lists';
    public const ENTITY_NAME =  UList::class;

    public function Get($blockId = '', $group = '', $order = ['ID' => 'ASC'])
    {
        return $this->GenMethod(__FUNCTION__, ['IBLOCK_TYPE_ID' => 'lists', 'IBLOCK_ID' => $blockId, 'SOCNET_GROUP_ID' => $group, 'IBLOCK_ORDER' => $order], $this::ENTITY_NAME);
    }

    public function SectionGet($blockId, $id = '', $group = '', $order = ['ID' => 'ASC'])
    {
        return $this->GenMethod(__FUNCTION__, ['IBLOCK_TYPE_ID' => 'lists', 'IBLOCK_ID' => $blockId, 'ELEMENT_ID' => $id, 'SOCNET_GROUP_ID' => $group, 'IBLOCK_ORDER' => $order], $this::ENTITY_NAME);
    }

    public function ElementGet($blockId, $id = '', $group = '', $order = ['ID' => 'ASC'])
    {
        return $this->GenMethod(__FUNCTION__, ['IBLOCK_TYPE_ID' => 'lists', 'IBLOCK_ID' => $blockId, 'ELEMENT_ID' => $id, 'SOCNET_GROUP_ID' => $group, 'IBLOCK_ORDER' => $order], $this::ENTITY_NAME);
    }
}
