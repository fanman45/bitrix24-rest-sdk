<?php

namespace Bitrix24RestSdk\Services;

use Bitrix24RestSdk\Services\AbstractService;

class UserfieldtypeService extends AbstractService
{
    public const API_PATH = 'userfieldtype';
    // public const ENTITY_NAME =  UList::class;

    public function Add($userTypeId, $handler, $title = null, $description = null, $options = null)
    {
        return $this->GenMethod(__FUNCTION__, ['USER_TYPE_ID' => $userTypeId, 'HANDLER' => $handler, 'TITLE' => $title, 'DESCRIPTION' => $description, 'OPTIONS' => $options]);
    }
    
    public function Delete($userTypeId)
    {
        return $this->GenMethod(__FUNCTION__, ['USER_TYPE_ID' => $userTypeId]);
    }

    public function List()
    {
        return $this->GenMethod(__FUNCTION__);
    }

    public function Update($userTypeId, $handler, $title, $description,$options)
    {
        return $this->GenMethod(__FUNCTION__, ['USER_TYPE_ID' => $userTypeId, 'HANDLER' => $handler, 'TITLE' => $title, 'DESCRIPTION' => $description, 'OPTIONS' => $options]);
    }

}
