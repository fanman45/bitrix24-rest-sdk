<?php

namespace Bitrix24RestSdk\Services;

use Bitrix24RestSdk\Services\AbstractService;

class EventService extends AbstractService
{
    public const API_PATH = 'event';

    public function Bind($event, $handler, $auth_type = null, $event_type = null, $auth_connector = null, $options = null)
    {
        return $this->GenMethod(__FUNCTION__, ['event' => $event, 'handler' => $handler, 'auth_type' => $auth_type, 'event_type' => $event_type, 'auth_connector' => $auth_connector, 'options' => $options]);
    }

    public function Get()
    {
        return $this->GenMethod(__FUNCTION__);
    }

    public function Unbind($event, $handler, $auth_type = null, $event_type = null,)
    {
        return $this->GenMethod(__FUNCTION__,  ['event' => $event, 'handler' => $handler, 'auth_type' => $auth_type, 'event_type' => $event_type]);
    }
}
