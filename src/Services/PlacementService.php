<?php

namespace Bitrix24RestSdk\Services;

use Bitrix24RestSdk\Services\AbstractService;

class PlacementService extends AbstractService
{
    public const API_PATH = 'placement';
    // public const ENTITY_NAME =  UList::class;

    /**
     * @param mixed $placement
     * @param mixed $handler
     * @param null $messageOut
     * @param null $tag
     * @param null $subTag
     * @param null $attach
     * 
     * 'PLACEMENT' => 'CRM_DEAL_DETAIL_TAB',
     *       'HANDLER' => 'https://dev2.d-corp.ru/pasport/main',
     *       'LANG_ALL' => [
     *           'en' => [
     *               'TITLE' => 'Паспорт(test)',
     *               'DESCRIPTION' => 'description',
     *               'GROUP_NAME' => 'group',
     *           ],
     *           'ru' => [
     *               'TITLE' => 'Паспорт(test)',
     *               'DESCRIPTION' => 'описание',
     *               'GROUP_NAME' => 'группа',
     *           ],
     *       ],
     */
    public function Bind($placement, $handler, $lang = null, $userId = null)
    {
        return $this->GenMethod(__FUNCTION__, ['PLACEMENT' => $placement, 'HANDLER' => $handler, 'LANG_ALL' => $lang, 'USER_ID' => $userId]);
    }

    public function Get()
    {
        return $this->GenMethod(__FUNCTION__);
    }

    public function List()
    {
        return $this->GenMethod(__FUNCTION__);
    }

    public function Unbind($placement, $handler){
        return $this->GenMethod(__FUNCTION__, ['PLACEMENT' => $placement, 'HANDLER' => $handler]);
    }
    
    // public function Delete($userTypeId)
    // {
    //     return $this->GenMethod(__FUNCTION__, ['USER_TYPE_ID' => $userTypeId]);
    // }

    // public function List()
    // {
    //     return $this->GenMethod(__FUNCTION__);
    // }

    // public function Update($userTypeId, $handler, $title, $description,$options)
    // {
    //     return $this->GenMethod(__FUNCTION__, ['USER_TYPE_ID' => $userTypeId, 'HANDLER' => $handler, 'TITLE' => $title, 'DESCRIPTION' => $description, 'OPTIONS' => $options]);
    // }

}