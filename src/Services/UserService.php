<?php

namespace Bitrix24RestSdk\Services;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\Crud;
use Bitrix24RestSdk\Core\Interfaces\CrudInterface;
use Bitrix24RestSdk\Core\RequestProcessor;
use Bitrix24RestSdk\Core\ResponseProcessor;
use Bitrix24RestSdk\Entity\User;

/**
 * Класс для работы с пользователями
 * Требует доработки!
 */
class UserService extends AbstractService implements CrudInterface
{
    use Crud;

    public const API_PATH = 'user';
    public const ENTITY_NAME = User::class;

    public function Current()
    {
        return $this->GenMethod(
            __FUNCTION__,
            [
                'auth_user' => [
                    'access_token' => $_REQUEST['AUTH_ID'],
                    'refresh_token' => $_REQUEST['REFRESH_ID'],
                    'application_token'  => $_REQUEST['APP_SID']
                ]
            ]
        );
    }

    public function Add(array $fields, array $parm = [])
    {
        return $this->GenMethod(__FUNCTION__, $fields, $parm);
    }

    public function Delete($id)
    {
        throw new \Exception('Method not exists');
    }

    /**
     * Закостылил типы - битрикс возвращает хрень
     * @return [type]
     */
    public function Fields()
    {
        $result = $this->GenMethod(__FUNCTION__);
        foreach ($result as $key => $field) {
            $result[$key] = [
                "type" => "string",
                "isRequired" => false,
                "isReadOnly" => true,
                "isImmutable" => false,
                "isMultiple" => false,
                "isDynamic" => false,
                "title" => $field,
            ];
        }
        return $result;
    }

    public function Search(array $filter = null)
    {
        return $this->GenMethod(__FUNCTION__, $filter);
    }
}
