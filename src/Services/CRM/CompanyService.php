<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\Crud;
use Bitrix24RestSdk\Core\Interfaces\CrudInterface;
use Bitrix24RestSdk\Entity\Company;

class CompanyService extends AbstractService implements CrudInterface
{
    use Crud;

    public const API_PATH = 'crm.company';
    public const ENTITY_NAME =  Company::class;

    public function Fields()
    {
        return $this->GenMethod(__FUNCTION__);
    }
}
