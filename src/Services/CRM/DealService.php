<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\Crud;
use Bitrix24RestSdk\Core\Interfaces\CrudInterface;
use Bitrix24RestSdk\Entity\Deal;
use Bitrix24RestSdk\Entity\Roll;

/**
 * Класс для работы с Сделками
 * Требует доработки!
 */
class DealService extends AbstractService implements CrudInterface
{
    use Crud;

    public const API_PATH = 'crm.deal';
    // public const ENTITY_NAME = Deal::class;
    // public const ENTITY_LIST = Roll::class;
    public function ExternalcallShow($id)
    {
        return $this->GenMethod(__FUNCTION__, ['ID' => $id]);
    }
    public function Fields()
    {
        return $this->GenMethod(__FUNCTION__);
    }
    public function UserfieldList($id)
    {
        return $this->GenMethod(__FUNCTION__, ['ID' => $id]);

        // $response = new ResponseProcessor($this->request->Call(__FUNCTION__, ['ID' => $id]));
        // $arrentity = [];
        // $entity = $this::ENTITY_LIST;
        // foreach ($response->Processing() as $enty) {
        //     $arrentity[] = new $entity($enty);
        // }
        // return  collect($arrentity);
    }

    public function ProductrowsGet($id)
    {
        return $this->GenMethod(__FUNCTION__, ['id' => $id]);
    }


    public function ProductrowsSet($id, array $rows)
    {
        // id: id, 
        // rows:
        // [ 
        //     { "PRODUCT_ID": 689, "PRICE": 100.00, "QUANTITY": 4 }, 
        //     { "PRODUCT_ID": 690, "PRICE": 400.00, "QUANTITY": 1 } 
        // ] 
        return $this->GenMethod(__FUNCTION__, ['id' => $id, 'rows' => $rows]);
    }
}
