<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;


class StatusService extends AbstractService
{

    public const API_PATH = 'crm.status';
    // public const ENTITY_NAME = Deal::class;

    public function Fields()
    {
        return $this->GenMethod(__FUNCTION__);
    }
    public function List($id = null)
    {
        return $this->GenMethod(__FUNCTION__, ['FILTER' => ['ENTITY_ID' => $id]]);
    }
}
