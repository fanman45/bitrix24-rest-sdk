<?php

namespace Bitrix24RestSdk\Services\CRM\Dynamic;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\CrudEntity;
use Bitrix24RestSdk\Entity\BaseItem;

class SettingService extends AbstractService
{
    use CrudEntity;

    public const API_PATH = 'crm.type';
    public const ENTITY_NAME =  BaseItem::class;

    public function Get(int $id)
    {
        return $this->GenMethod(__FUNCTION__, ['id' => $id]);
    }

    // public function Add(int $entityTypeId, array $fields)
    // {
    //     return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'fields' => $fields]);
    // }

    // public function Delete(int $entityTypeId, int $id)
    // {
    //     return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id]);
    // }

    // public function Get(int $entityTypeId, int $id,$crudEntity = null)
    // {
    //     return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id],$crudEntity);
    // }

    public function List(int $entityTypeId = null, array $fields = null)
    {
        return $this->GenMethod(__FUNCTION__,['entityTypeId' => $entityTypeId, 'fields' => $fields]);
    }

    public function Fields()
    {
        return $this->GenMethod(__FUNCTION__);
    }
}
