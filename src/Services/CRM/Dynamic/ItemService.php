<?php

namespace Bitrix24RestSdk\Services\CRM\Dynamic;

use Bitrix24RestSdk\Services\AbstractService;
// use Bitrix24RestSdk\Core\Traits\CrudEntity;
use Bitrix24RestSdk\Entity\Item;

class ItemService extends AbstractService
{
    // use CrudEntity;

    public const API_PATH = 'crm.item';
    public const ENTITY_NAME = Item::class;

    public function Add(int $entityTypeId, array $fields)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'fields' => $fields]);
    }

    public function Update(int $entityTypeId, int $id, array $fields)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id, 'fields' => $fields]);
    }

    public function Delete(int $entityTypeId, int $id)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id]);
    }

    public function Get(int $entityTypeId, int $id, $crudEntity = null)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id], $crudEntity);
    }

    public function List(int $entityTypeId, array|null $select = null, array|null $order = null, array|null $filter = null, int|null $start = 0)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'select' => $select, 'order' => $order, 'filter' => $filter, 'start' => $start,]);
    }

    public function Fields($entityTypeId)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId]);
    }
}
