<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;

class ProductService extends AbstractService
{
    public const API_PATH = 'crm.product';
    // public const ENTITY_NAME =  Lead::class;

    public function Get(int $id)
    {
        return $this->GenMethod(__FUNCTION__, ['id' => $id]);
    }
    public function Fields()
    {
        return $this->GenMethod(__FUNCTION__);
    }
}
