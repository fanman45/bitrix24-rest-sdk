<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\CrudEntity;
use Bitrix24RestSdk\Entity\Category;

/**
 *  ИД типов https://dev.1c-bitrix.ru/api_d7/bitrix/crm/crm_owner_type/identifiers.php
 * [Description CategoryService]
 */
class CategoryService extends AbstractService
{
    use CrudEntity;
    public const API_PATH = 'crm.category';
    public const ENTITY_NAME =  Category::class;

    public function Add(int $entityTypeId, array $fields)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'fields' => $fields]);
    }

    public function Delete(int $entityTypeId, int $id)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id]);
    }

    public function Get(int $entityTypeId, int $id)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId, 'id' => $id]);
    }

    public function List(int $entityTypeId)
    {
        return $this->GenMethod(__FUNCTION__, ['entityTypeId' => $entityTypeId]);
    }
}
