<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\Crud;
use Bitrix24RestSdk\Core\Interfaces\CrudInterface;
use Bitrix24RestSdk\Entity\Lead;

class LeadService extends AbstractService implements CrudInterface
{
    use Crud;

    public const API_PATH = 'crm.lead';
    public const ENTITY_NAME =  Lead::class;
}
