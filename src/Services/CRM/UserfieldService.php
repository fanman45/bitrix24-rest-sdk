<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\Crud;
use Bitrix24RestSdk\Core\Interfaces\CrudInterface;
use Bitrix24RestSdk\Entity\Userfield;

class UserfieldService extends AbstractService implements CrudInterface
{
    use Crud;

    public const API_PATH = 'crm.userfield';
    public const ENTITY_NAME =  Userfield::class;

    public function Fields()
    {
        return $this->GenMethod(__FUNCTION__);
    }
    public function Types()
    {
        return $this->GenMethod(__FUNCTION__);
    }
}
