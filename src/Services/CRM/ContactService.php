<?php

namespace Bitrix24RestSdk\Services\CRM;

use Bitrix24RestSdk\Services\AbstractService;
use Bitrix24RestSdk\Core\Traits\Crud;
use Bitrix24RestSdk\Core\Interfaces\CrudInterface;
use Bitrix24RestSdk\Entity\Contact;

class ContactService extends AbstractService implements CrudInterface
{
    use Crud;

    public const API_PATH = 'crm.contact';
    public const ENTITY_NAME =  Contact::class;

    public function Fields()
    {
        return $this->GenMethod(__FUNCTION__);
    }
}
