<?php

namespace Bitrix24RestSdk\Services;
use Bitrix24RestSdk\Entity\Iblock;

class IblockService extends AbstractService
{
    public const API_PATH = 'iblock.Element';
    public const ENTITY_NAME =  Iblock::class;
    // добавить фильтры
    public function Get($iblockId, $elementId, $select = null)
    {
    return $this->GenMethod(__FUNCTION__, ['iblockId' => $iblockId, 'elementId' => $elementId, 'select' => $select]/*, $this::ENTITY_NAME*/);
    }
    // iblock.Element.list(iblockId, select, filter, order, pageNavigation)
    public function List($iblockId, $pageNavigation = null, $filter = null, $select = null, $order = null)
    {
    return $this->GenMethod(__FUNCTION__, ['iblockId' => $iblockId, 'select' => $select, 'elementId' => $filter, 'order' => $order, 'pageNavigation' => $pageNavigation]/*, $this::ENTITY_NAME*/);
    }
}
